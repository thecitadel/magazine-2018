<?php

add_theme_support( 'title-tag' );
add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 1568, 9999 );
add_theme_support(
	'html5',
	array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	)
);
add_theme_support(
	'custom-logo',
	array(
		'height'      => 102,
		'width'       => 540,
		'flex-width'  => true,
		'flex-height' => true,
	)
);
// Add theme support for selective refresh for widgets.
add_theme_support( 'customize-selective-refresh-widgets' );

// Add support for Block Styles.
add_theme_support( 'wp-block-styles' );

// Add support for full and wide align images.
add_theme_support( 'align-wide' );

// Add support for editor styles.
add_theme_support( 'editor-styles' );

// Enqueue editor styles.
add_editor_style( 'style-editor.css' );

// Add support for responsive embedded content.
add_theme_support( 'responsive-embeds' );

/**
 * Enqueue scripts and styles.
 */
function magazine_scripts() {
	// Get the theme data
	$the_theme     = wp_get_theme();
	$theme_version = $the_theme->get( 'Version' );
	
	$css_version = $theme_version . '.' . filemtime( get_template_directory() . '/style.css' );
	wp_enqueue_style( 'magazine-2018-styles', get_template_directory_uri() . '/style.css', array(), $css_version );
	wp_enqueue_style( 'font-awesome', 'https://use.fontawesome.com/releases/v5.8.2/css/all.css', array(), $theme_version );
	wp_enqueue_style( 'citadel-fonts', 'https://use.typekit.net/onz2qme.css', array(), $theme_version );
	
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'magazine_scripts' );
