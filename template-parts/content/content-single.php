
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if ( has_post_thumbnail() ) : ?>
	<header class="entry-header" style="background-image: url('<?php echo get_the_post_thumbnail_url() ?>'); background-position: center left;">
		<div class="mag-content-table container">
			<div class="magazine-header-content right">
				<h1 class="section-title cit-fade-in">
					<span class="mag-first-title">A Dream</span>
					<span class="mag-second-title">Fulfilled</span>
				</h1><!-- .section-title -->
				<p class="magazine-deck cit-fade-in">Cadet Col. Sarah Zorn has been in the spotlight since she became the first female regimental commander of the Corps of Cadets. Behind that historical benchmark lies a remarkable young woman with a resonating story about overcoming adversity and climbing to the top through resilience and determination.</p>
				<p class="magazine-byline cit-fade-in">by <span class="uppercase">Cadet Colonel Sarah Zorn</span></p>
				<button class="view-story cit-bounce" title="View Story"><span class="screen-reader-text">View Story</span><i class="fas fa-chevron-down fa-fw"></i></button>
			</div>
		</div>
	</header>
	<?php endif; ?>

	<div class="entry-content">
		<div class="story-container">
			<?php the_content(); ?>
		</div>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
	
	</footer><!-- .entry-footer -->

</article><!-- #post-<?php the_ID(); ?> -->
