<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'twentynineteen' ); ?></a>

		<header id="masthead">
		
			<div class="site-branding container">
				<?php
				if (is_home() || is_front_page()) :
				?>
				
				<h1>
					<?php 
					if ( has_custom_logo() ) :
						the_custom_logo();
					endif; 
					?>
				</h1>
				
				<?php 
				else : 
					if ( has_custom_logo() ) :
						the_custom_logo();
					endif; 
				endif;
				?>
			</div>

		</header><!-- #masthead -->

	<div id="content" class="site-content">
